
import Foundation

struct Movie: Codable {
    
    let id: Int
    let title: String
    let releaseYear: Int
    let duration: Int
    let coverImageUrl: String?
    let likes: Int
    let ranking: Double

    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case releaseYear = "release_year"
        case duration = "duration"
        case coverImageUrl = "cover_image_url"
        case likes = "likes"
        case ranking = "rank"
    }
}
