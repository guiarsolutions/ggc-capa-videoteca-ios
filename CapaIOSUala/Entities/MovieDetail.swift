
import Foundation

struct MovieDetail: Codable {
    
    let title: String
    let description: String
    let userMovie: UserMovie
    let genders: [Gender]
    let directors: [Director]
    let starring: [Starring]
    
    private enum CodingKeys: String, CodingKey {
        case title = "title"
        case description = "description"
        case userMovie = "user_movie_data"
        case genders = "genders"
        case directors = "directors"
        case starring = "starring"
    }
}
