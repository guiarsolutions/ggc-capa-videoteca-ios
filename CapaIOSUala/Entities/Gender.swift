
import Foundation

struct Gender: Codable {
    
    let id: Int
    let name: String

    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }
}
