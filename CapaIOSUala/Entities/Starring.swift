
import Foundation

struct Starring: Codable {
    
    let id: Int
    let firstName: String
    let lastName: String
    let pictureUrl: String
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case firstName = "firstname"
        case lastName = "lastname"
        case pictureUrl = "picture_url"
    }
}
