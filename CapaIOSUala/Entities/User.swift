
import Foundation

struct User: Codable {
    
    let id: Int
    let username: String
    let dni: String
    let firstName: String
    let lastName: String
    let email: String?
    let birthDateString: String?
    
    var birthDate: Date? {
        
        guard let dateString = birthDateString else {
            return nil
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
    
        return formatter.date(from: dateString)
    }
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case username = "username"
        case dni = "dni"
        case firstName = "first_name"
        case lastName = "last_name"
        case email = "email"
        case birthDateString = "birth_date"
    }
}
