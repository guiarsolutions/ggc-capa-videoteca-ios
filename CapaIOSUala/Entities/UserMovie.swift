
import Foundation

struct UserMovie: Codable {
    
    let like: Bool
    let vote: Double
    
    private enum CodingKeys: String, CodingKey {
        case like = "user_like"
        case vote = "user_vote"
    }
}
