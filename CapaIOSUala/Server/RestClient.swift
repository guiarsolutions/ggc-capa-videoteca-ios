import Alamofire
import RxAlamofire
import RxSwift

class RestClient {
    
    fileprivate let host: String
    fileprivate let timeout: TimeInterval = 30.0
    
    required init(host: String) {
        
        self.host = host
    }
    
    func get(endPoint: EndPoint,
             urlParams: [String: Any]? = nil) -> Observable<Any> {
        
        var urlString = host + endPoint.rawValue
        
        if let params = urlParams {
            urlString.append("?")
            
            for (k, v) in params {
                urlString.append("\(k)=\(v)&")
            }
        }
        
        guard
            let urlRequest = urlRequestFromString(urlString,
                                                  method: .get) else {
                                                    
                                                    return Observable.empty()
        }
        
        return observableRequest(urlRequest)
    }
    
    func post(endPoint: EndPoint,
              jsonBody: [String: Any]? = nil) -> Observable<Any> {
        
        let urlString = host + endPoint.rawValue
        
        guard
            var urlRequest = urlRequestFromString(urlString,
                                                  method: .post) else {
                                                    
                                                    return Observable.empty()
        }
        
        if let bodyParams = jsonBody {
            
            guard
                let data = try? JSONSerialization.data(withJSONObject: bodyParams,
                                                       options: .fragmentsAllowed) else {
                                                        
                                                        return Observable.empty()
            }
            
            urlRequest.httpBody = data
        }
        
        return observableRequest(urlRequest)
    }
}

extension RestClient {
    
    fileprivate func urlRequestFromString(_ stringUrl: String, method: HTTPMethod) -> URLRequest? {
        
        guard let url = URL(string: stringUrl) else {
            
            return nil
        }
        
        var urlRequest = URLRequest(url: url,
                                    cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                    timeoutInterval: timeout)
        
        urlRequest.allHTTPHeaderFields = ["Content-type" : "application/json; charset=utf-8"]
        urlRequest.httpMethod = method.rawValue
        
        return urlRequest
    }
    
    fileprivate func observableRequest(_ urlRequest: URLRequest) -> Observable<Any> {
        
        let host = self.host
        
        return RxAlamofire.requestJSON(urlRequest).flatMap { (object) -> Observable<Any> in
            
            let (response, json) = object
            
            if response.statusCode != 200 {
                
                let error = NSError(domain: host,
                                    code: response.statusCode,
                                    userInfo: [NSLocalizedDescriptionKey: "Unable to connect to server"])
                
                return Observable.error(error)
            }
            
            let serverError = NSError(domain: host,
                                      code: response.statusCode,
                                      userInfo: [NSLocalizedDescriptionKey: "Server Error - Invalid Response"])
            
            guard
                let jsonData = try? JSONSerialization.data(withJSONObject: json,
                                                           options: .prettyPrinted),
                let jsonDict = try? JSONSerialization.jsonObject(with: jsonData,
                                                                 options: []) as? [String: Any] else {
                                                            
                                                            return Observable.error(serverError)
            }
            
            if let clientErrorObj = jsonDict["error"],
                let clientErrorData = try? JSONSerialization.data(withJSONObject: clientErrorObj,
                                                                 options: .prettyPrinted),
                let clientError = try? JSONDecoder().decode(ClientError.self, from: clientErrorData) {
                
                let clientError = NSError(domain: host,
                                          code: clientError.code,
                                          userInfo: [NSLocalizedDescriptionKey: clientError.message])
                
                return Observable.error(clientError)
            }
            
            return Observable.just(jsonDict["result"] ?? json)
            
        }.asObservable()
    }
}
