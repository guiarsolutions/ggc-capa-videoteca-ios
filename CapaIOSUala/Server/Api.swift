
enum Server: String {
    
    case production = "https://capa-ios-api.herokuapp.com/api"
    case testing = "<url a testing>"
}

enum EndPoint: String {
    
    case login = "/user/login"
    case editUser = "/user/edit"
    case getAllMovies = "/movies/getAll"
    case getMovieDetail = "/movies/getMovieDetail"
    case getMoviesLikedForUser = "/movies/getMoviesLikedForUser"
    case voteMovie = "/movies/voteMovie"
}

class Api {
    
    private let server: Server
    
    required init(server: Server) {
        self.server = server
    }
    
    var client: RestClient { return RestClient(host: server.rawValue) }

    class func production() -> Api {
        return Api(server: .production)
    }
    
    class func testing() -> Api {
        return Api(server: .testing)
    }
}
