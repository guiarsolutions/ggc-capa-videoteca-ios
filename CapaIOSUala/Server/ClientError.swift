
import Foundation

struct ClientError: Codable {
    
    let code: Int
    let message: String
    
    private enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
    }
}
