
import RxSwift

class MoviesRepository {
    
    func getAll() -> Observable<[Movie]> {
        
        let client = Api.production().client
        
        return client.get(endPoint: .getAllMovies)
            .flatMap({ (response) -> Observable<[Movie]> in
                
                guard
                    let jsonData = try? JSONSerialization.data(withJSONObject: response,
                                                               options: .prettyPrinted),
                    let movies = try? JSONDecoder().decode([Movie].self, from: jsonData) else {
                        
                        return Observable.just([])
                }
                
                return Observable.just(movies)
            })
    }
    
    func getMovieDetail(movieId: Int,
                        userId: Int) -> Observable<MovieDetail?> {
        
        let client = Api.production().client
        
        let params = ["movieId": movieId,
                      "userId": userId]
        
        return client.get(endPoint: .getMovieDetail, urlParams: params)
            .flatMap({ (response) -> Observable<MovieDetail?> in
                
                guard
                    let jsonData = try? JSONSerialization.data(withJSONObject: response,
                                                               options: .prettyPrinted),
                    let movieDetail = try? JSONDecoder().decode(MovieDetail.self, from: jsonData) else {
                        
                        return Observable.just(nil)
                }
                
                return Observable.just(movieDetail)
            })
    }
    
    func getMoviesLikedForUser(userId: Int) -> Observable<[Movie]> {
        
        let client = Api.production().client
        
        let params = ["userId": userId]
        
        return client.get(endPoint: .getMoviesLikedForUser, urlParams: params)
            .flatMap({ (response) -> Observable<[Movie]> in
                
                guard
                    let jsonData = try? JSONSerialization.data(withJSONObject: response,
                                                               options: .prettyPrinted),
                    let movies = try? JSONDecoder().decode([Movie].self, from: jsonData) else {
                        
                        return Observable.just([])
                }
                
                return Observable.just(movies)
            })
    }
    
    func voteMovie(movieId: Int,
                   userId: Int,
                   vote: Double) -> Observable<MovieDetail?> {
        
        let client = Api.production().client
        
        let params = ["userId": userId,
                      "movieId": movieId,
                      "vote": vote] as [String : Any]
        
        return client.post(endPoint: .voteMovie, jsonBody: params)
            .flatMap({ (response) -> Observable<MovieDetail?> in
                
                guard
                    let jsonData = try? JSONSerialization.data(withJSONObject: response,
                                                               options: .prettyPrinted),
                    let movieDetail = try? JSONDecoder().decode(MovieDetail.self, from: jsonData) else {
                        
                        return Observable.just(nil)
                }
                
                return Observable.just(movieDetail)
            })
    }
}
