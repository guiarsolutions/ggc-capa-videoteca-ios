
import RxSwift

class LoginError: NSError {
    
    override var localizedDescription: String {
        return "No existe usuario para los datos ingresados."
    }
}

class EditUserError: NSError {
    
    override var localizedDescription: String {
        return "No se pudo recuperar los datos del usuario"
    }
}

class UserRepository {
        
    func login(username: String,
               dni: String) -> Observable<User> {
        
        let client = Api.production().client
        
        let params = ["username": username,
                      "dni": dni]
        
        return client.post(endPoint: .login, jsonBody: params).flatMap({ (response) -> Observable<User> in
            
            guard
                let jsonData = try? JSONSerialization.data(withJSONObject: response,
                                                           options: .prettyPrinted),
                let user = try? JSONDecoder().decode(User.self, from: jsonData) else {
                    
                    return Observable.error(LoginError())
            }
                            
            return Observable.just(user)
        })
    }
    
    func editUser(userId: Int,
                  email: String?,
                  birthDate: Date?) -> Observable<User> {
        
        let client = Api.production().client
        
        var params = ["userId": userId,
                      "email": "",
                      "birth_date": ""] as [String: Any]
        
        if let email = email,
            !email.isEmpty {
            
            params["email"] = email
        }
        
        if let birthDate = birthDate {
            
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd"
            params["birth_date"] = format.string(from: birthDate)
        }
        
        return client.post(endPoint: .editUser, jsonBody: params).flatMap({ (response) -> Observable<User> in
            
            guard
                let jsonData = try? JSONSerialization.data(withJSONObject: response,
                                                           options: .prettyPrinted),
                let user = try? JSONDecoder().decode(User.self, from: jsonData) else {
                    
                    return Observable.error(EditUserError())
            }
                            
            return Observable.just(user)
        })
    }
}

