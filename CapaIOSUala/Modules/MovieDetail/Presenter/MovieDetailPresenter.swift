
import UIKit

protocol MovieDetailPresentable: UIViewController {
    
    func showLoading()
    func hideLoading()
    func showError(_ error: Error)
    func showMovie(_ movie: Movie)
    func showDetail(_ movieDetail: MovieDetail?)
}

class MovieDetailPresenter {
    
    private let interactor = MovieDetailInteractor()
    
    private let router: MovieDetailRouter
    
    weak var view: MovieDetailPresentable? {
        didSet {
            view?.showMovie(movie)
        }
    }
    
    private var cache: MovieDetail?
    
    private let movie: Movie
    
    init(movie: Movie,
         router: MovieDetailRouter) {
        self.movie = movie
        self.router = router
        interactor.delegate = self
    }
    
    func getMovieDetail() {
        
        if let detail = cache {
            view?.showDetail(detail)
            return
        }
        
        guard let user = Session.getUser() else {
            return
        }
        
        view?.showLoading()
        
        interactor.getDetail(movieId: movie.id, userId: user.id)
    }
    
    func voteMovie(from view: UIViewController) {
        
        guard let detail = cache else {
            return
        }
        
        router.voteMovie(movie: movie,
                         userMovie: detail.userMovie,
                         delegate: self,
                         from: view)
    }
}

extension MovieDetailPresenter: UserLikeMovieChangesDelegate {
    
    func didChangeMovieDetail(_ movieDetail: MovieDetail) {
        
        cache = movieDetail
        
        view?.showDetail(movieDetail)
    }
}

extension MovieDetailPresenter: MovieDetailInteractorDelegate {
    
    func didFail(error: Error) {
        
        view?.hideLoading()
        
        view?.showError(error)
    }
    
    func didResponse(detail: MovieDetail?) {
        
        self.cache = detail
        
        view?.hideLoading()
        
        view?.showDetail(detail)
    }
}
