
import UIKit

class MovieDetailViewController: UIViewController {

    @IBOutlet var table: UITableView?
    
    @IBOutlet var coverImageView: UIImageView?
    @IBOutlet var titleLabel: UILabel?
    @IBOutlet var durationLabel: UILabel?
    @IBOutlet var releaseYearLabel: UILabel?
    @IBOutlet var descriptionLabel: UILabel?
    
    private var detail: MovieDetail?
    private let presenter: MovieDetailPresenter
    
    private lazy var loading: UIViewController = {
        let loading = UIAlertController(title: "Descargando..",
                                        message: nil,
                                        preferredStyle: .alert)
        return loading
    }()
    
    private lazy var voteItem: UIBarButtonItem = {
       
        let item = UIBarButtonItem(title: "Votar",
                                   style: .done,
                                   target: self,
                                   action: #selector(voteMovie))
        return item
    }()
    
    private var showingLoading = false
    
    init(presenter: MovieDetailPresenter) {
        self.presenter = presenter
        super.init(nibName: "MovieDetailViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clear()
        setup()
    }

    private func setup() {
        
        title = "Detalle"
        presenter.view = self

        setupTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.getMovieDetail()
    }
    
    private func clear() {
        
        titleLabel?.text = "-"
        durationLabel?.text = "-"
        releaseYearLabel?.text = "-"
        descriptionLabel?.text = "-"
    }
    
    private func setupTable() {
        
        table?.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        table?.bounces = false
        table?.separatorStyle = .none
        
        table?.register(UINib(nibName: "MovieDetailCell", bundle: nil),
                        forCellReuseIdentifier: MovieDetailCell.reuseIdentifier)
        
        table?.delegate = self
        table?.dataSource = self
        
        setupVoteItem()
    }
    
    private func setupVoteItem() {
        
        voteItem.isEnabled = false
        navigationItem.rightBarButtonItem = voteItem
    }
    
    @objc private func voteMovie() {
        
        presenter.voteMovie(from: self)
    }
}

extension MovieDetailViewController: MovieDetailPresentable {
    
    func showLoading() {
        
        guard !showingLoading else {
            return
        }
        
        showingLoading = true
        
        present(loading, animated: true, completion: nil)
    }
    
    func hideLoading() {
        
        guard showingLoading else {
            return
        }
        
        loading.dismiss(animated: true) { [weak self] in
            self?.showingLoading = false
        }
    }
    
    func showError(_ error: Error) {
                
        let alert = UIAlertController(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Aceptar", style: .cancel) { (action) in
            
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }

    func showMovie(_ movie: Movie) {
        
        if let coverImageUrl = movie.coverImageUrl {
            coverImageView?.sd_setImage(with: URL(string: coverImageUrl), completed: nil)
        }
        titleLabel?.text = movie.title
        releaseYearLabel?.text = "\(movie.releaseYear)"
        durationLabel?.text = "\(movie.duration/60)h \(movie.duration%60)min"
    }
    
    func showDetail(_ movieDetail: MovieDetail?) {
        
        detail = movieDetail
        
        descriptionLabel?.text = movieDetail?.description
        voteItem.isEnabled = movieDetail != nil
        
        table?.reloadData()
    }
}

extension MovieDetailViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return detail != nil ? 3 : 0
    }
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat {
        
        return 34
    }
    
    func tableView(_ tableView: UITableView,
                   viewForHeaderInSection section: Int) -> UIView? {
        
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = .blue
        
        if section == 0 {
            label.text = "Genero"
        }
        else if section == 1 {
            label.text = "Director/es"
        }
        else {
            label.text = "Protagonista/s"
        }
        
        return label
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 34
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return detail?.genders.count ?? 0
        }
        else if section == 1 {
            
            return detail?.directors.count ?? 0
        }
        else {
            
            return detail?.starring.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: MovieDetailCell.reuseIdentifier,
                                                 for: indexPath)
        
        if let detailCell = cell as? MovieDetailCell {
            
            let section = indexPath.section
            
            if section == 0,
                let gender = detail?.genders[indexPath.row] {
                
                detailCell.titleButton?.setTitle(gender.name,
                                                 for: .normal)
            }
            else if section == 1,
                let director = detail?.directors[indexPath.row] {
             
                let fullName = "\(director.firstName) \(director.lastName)"
                
                detailCell.titleButton?.setTitle(fullName,
                                                 for: .normal)
            }
            else if let starring = detail?.starring[indexPath.row] {
                
                let fullName = "\(starring.firstName) \(starring.lastName)"
                
                detailCell.titleButton?.setTitle(fullName,
                                                 for: .normal)
            }
        }
        
        return cell
    }
    
}
