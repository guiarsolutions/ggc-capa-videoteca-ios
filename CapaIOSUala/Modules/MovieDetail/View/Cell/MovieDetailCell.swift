
import UIKit

class MovieDetailCell: UITableViewCell {

    @IBOutlet var titleButton: UIButton?
    
    static let reuseIdentifier = "MovieDetailCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        
        titleButton?.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        titleButton?.layer.cornerRadius = 5
    }

    private func clear() {
        
        titleButton?.setTitle("", for: .normal)
    }
}
