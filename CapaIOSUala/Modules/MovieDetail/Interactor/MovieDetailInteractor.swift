
import RxSwift

protocol MovieDetailInteractorDelegate : class {
    
    func didFail(error: Error)
    func didResponse(detail: MovieDetail?)
}

class MovieDetailInteractor {
    
    weak var delegate: MovieDetailInteractorDelegate?
    
    private var disposable: Disposable?
    
    private let repository = MoviesRepository()
    
    func getDetail(movieId: Int,
                   userId: Int) {
        
        disposable?.dispose()
        
        disposable = repository.getMovieDetail(movieId: movieId, userId: userId)
            .subscribe(onNext: { [weak self] (detail) in
                
                self?.delegate?.didResponse(detail: detail)
                
                }, onError: { [weak self] (error) in
                    
                    self?.delegate?.didFail(error: error)
                    
                }, onCompleted: nil, onDisposed: nil)
    }
}
