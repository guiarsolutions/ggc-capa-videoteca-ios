
import UIKit

class MovieDetailRouter {
    
    static func start(movie: Movie) -> UIViewController {
        
        let presenter = MovieDetailPresenter(movie: movie, router: MovieDetailRouter())
        
        let controller = MovieDetailViewController(presenter: presenter)
        
        return controller
    }
    
    func voteMovie(movie: Movie,
                   userMovie: UserMovie,
                   delegate: UserLikeMovieChangesDelegate,
                   from view: UIViewController) {
        
        let controller = UserLikeMovieRouter.start(movie: movie,
                                                   userMovie: userMovie,
                                                   delegate: delegate)
        
        view.present(controller, animated: true, completion: nil)
    }
}
