
import UIKit

protocol EditUserPresentable: UIViewController {
    
    func showLoading()
    func hideLoading()
    func showError(_ error: Error)
    func editUserSuccessfull(user: User)
}

class EditUserPresenter {
    
    private let interactor = EditUserInteractor()
    
    private let router: EditUserRouter
    
    weak var view: EditUserPresentable?
    
    init(router: EditUserRouter) {
        self.router = router
        interactor.delegate = self
    }
    
    func getUser() -> User? {
        
        return Session.getUser()
    }
    
    func updateAccount(email: String?,
                       birthDate: Date?) {
        
        guard let user = Session.getUser() else {
            return
        }
        
        view?.showLoading()
        
        interactor.execute(userId: user.id,
                           email: email,
                           birthdate: birthDate)
    }
}

extension EditUserPresenter: EditUserInteractorDelegate {
    
    func didFail(error: Error) {
        
        view?.hideLoading()
        
        view?.showError(error)
    }
    
    func didEdit(user: User) {
        
        view?.hideLoading()
        
        view?.editUserSuccessfull(user: user)
    }
}
