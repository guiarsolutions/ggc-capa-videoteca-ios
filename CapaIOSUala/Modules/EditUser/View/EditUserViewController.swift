
import UIKit

class EditUserViewController: UIViewController {

    @IBOutlet var email: UITextField?
    @IBOutlet var birthdate: UITextField?
    
    @IBOutlet var updateButton: UIButton?
    @IBOutlet var loading: UIActivityIndicatorView?
    
    private lazy var datePicker: UIDatePicker = {
       
        let picker = UIDatePicker()
        picker.date = presenter.getUser()?.birthDate ?? Date()
        picker.datePickerMode = .date
        picker.calendar = Calendar.current

        return picker
    }()
    
    private lazy var dateFormatter: DateFormatter = {
        
        let format = DateFormatter()
        format.dateFormat = "dd/MM/yyyy"
        
        return format
    }()
    
    private let presenter: EditUserPresenter
    
    private var selectedDate: Date? {
        didSet {
            guard let date = selectedDate else {
                birthdate?.text = ""
                return
            }
            birthdate?.text = dateFormatter.string(from: date)
        }
    }
    
    init(presenter: EditUserPresenter) {
        self.presenter = presenter
        super.init(nibName: "EditUserViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Editar Cuenta"
        style()
        setup()
    }
    
    private func style() {
        
        updateButton?.layer.cornerRadius = 5
        updateButton?.layer.masksToBounds = true
    }

    private func setup() {
        
        presenter.view = self
        
        email?.autocorrectionType = .no
        email?.placeholder = "email@dominio.com"
        email?.keyboardType = .emailAddress
        
        birthdate?.placeholder = "DD/MM/YYYY"
        birthdate?.inputView = datePicker
        
        datePicker.addTarget(self,
                             action: #selector(didChangeDate(picker:)),
                             for: .valueChanged)
        
        updateButton?.addTarget(self,
                                action: #selector(updateAccount),
                                for: .touchUpInside)
        
        hideLoading()
        
        setupInputs()
    }
    
    @objc private func updateAccount() {
        
        guard validate() else {
            return
        }
        
        view.endEditing(true)
        disableInputs()
        
        updateButton?.isHidden = true
        
        presenter.updateAccount(email: email?.text,
                                birthDate: selectedDate)
    }
    
    private func setupInputs() {
        
        guard let user = presenter.getUser() else {
            return
        }
        
        email?.text = user.email
        selectedDate = user.birthDate
        
        enableInputs()
    }
    
    private func disableInputs() {
        
        email?.isEnabled = false
        birthdate?.isEnabled = false
    }
    
    private func enableInputs() {
        
        email?.isEnabled = true
        birthdate?.isEnabled = true
    }
    
    @objc private func didChangeDate(picker: UIDatePicker) {
        
        selectedDate = picker.date
    }
    
    private func validate() -> Bool {
        
        return true
    }
}

extension EditUserViewController : EditUserPresentable {
    
    func hideLoading() {
        
        loading?.stopAnimating()
        loading?.isHidden = true
    }
    
    func showLoading() {
        
        loading?.isHidden = false
        loading?.startAnimating()
    }
    
    func showError(_ error: Error) {
        
        let alert = UIAlertController(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Aceptar", style: .cancel) { (action) in
            
            alert.dismiss(animated: true, completion: {
            
                self.setupInputs()
            })
        }
        
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func editUserSuccessfull(user: User) {
        
        let alert = UIAlertController(title: "Se actualizo la cuenta con exito!",
                                      message: nil,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Aceptar", style: .default) { (action) in
            
            alert.dismiss(animated: true, completion: {
            
                self.navigationController?.popViewController(animated: true)
            })
        }
        
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
}
