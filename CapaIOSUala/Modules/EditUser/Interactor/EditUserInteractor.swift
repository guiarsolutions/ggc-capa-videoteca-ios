
import RxSwift

protocol EditUserInteractorDelegate : class {
    
    func didFail(error: Error)
    func didEdit(user: User)
}

class EditUserInteractor {
    
    weak var delegate: EditUserInteractorDelegate?
    
    private var disposable: Disposable?
    
    private let repository = UserRepository()
    
    func execute(userId: Int,
                 email: String?,
                 birthdate: Date?) {
        
        disposable?.dispose()
        
        disposable = repository.editUser(userId: userId,
                                         email: email,
                                         birthDate: birthdate).subscribe(onNext: { [weak self] (user) in
                                            
                                            Session.updateUser(user)
                                            
                                            self?.delegate?.didEdit(user: user)
                                            
                                            }, onError: { [weak self] (error) in
                                                
                                                self?.delegate?.didFail(error: error)
                                                
                                            }, onCompleted: nil, onDisposed: nil)
    }
}
