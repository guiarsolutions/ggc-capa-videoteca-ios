
import UIKit

class EditUserRouter {
    
    static func start() -> UIViewController {
        
        let presenter = EditUserPresenter(router: EditUserRouter())
        
        let controller = EditUserViewController(presenter: presenter)
        
        return controller
    }
}
