
import RxSwift

protocol MoviesLibraryInteractorDelegate : class {
    
    func didFail(error: Error)
    func didResponse(movies: [Movie])
}

class MoviesLibraryInteractor {
    
    weak var delegate: MoviesLibraryInteractorDelegate?
    
    private var disposable: Disposable?
    
    private let repository = MoviesRepository()
    
    func getAll() {
        
        disposable?.dispose()
        
        disposable = repository.getAll()
            .subscribe(onNext: { [weak self] (movies) in
                
                self?.delegate?.didResponse(movies: movies)
                
                }, onError: { [weak self] (error) in
                    
                    self?.delegate?.didFail(error: error)
                    
                }, onCompleted: nil, onDisposed: nil)
    }
}
