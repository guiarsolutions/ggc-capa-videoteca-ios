
import UIKit
import SDWebImage

class MovieLibraryCell: UICollectionViewCell {

    @IBOutlet var cover: UIImageView?
    @IBOutlet var title: UILabel?
    @IBOutlet var likes: UILabel?
    @IBOutlet var ranking: UILabel?
    @IBOutlet var year: UILabel?
        
    static let reuseIdentifier = "MovieLibraryCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }
    
    private func setup() {
                
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 2.0
        layer.cornerRadius = 5
    }
    
    private func clear() {
        
        cover?.image = nil
        likes?.text = "-"
        title?.text = "-"
        ranking?.text = "-"
        year?.text = "-"
    }

    func setup(movie: Movie) {
        
        if let coverImageUrl = movie.coverImageUrl {
            cover?.sd_setImage(with: URL(string: coverImageUrl), completed: nil)
        }
        title?.text = movie.title
        year?.text = "\(movie.releaseYear)"
        likes?.text = "\(movie.likes)"
        ranking?.text = "\(movie.ranking)"
    }
}
