
import UIKit

class MoviesLibraryViewController: UIViewController {

    @IBOutlet var collection: UICollectionView?
    
    private var movies = [Movie]()
    
    private let interItemSpacing: CGFloat = 8
    private let lineSpacing: CGFloat = 8
    private let itemAspect: CGFloat = 1/1.5
    
    private let presenter: MoviesLibraryPresenter
    
    private lazy var refreshControl = UIRefreshControl()
    
    private lazy var loading: UIViewController = {
        let loading = UIAlertController(title: "Descargando..",
                                        message: nil,
                                        preferredStyle: .alert)
        return loading
    }()
    
    private var showingLoading = false
    
    init(presenter: MoviesLibraryPresenter) {
        self.presenter = presenter
        super.init(nibName: "MoviesLibraryViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    private func setup() {
        
        title = NSLocalizedString("movie_libray_title",
                                  comment: "")
        presenter.view = self
                
        refreshControl.addTarget(self,
                                 action: #selector(refresh),
                                 for: .valueChanged)
        
        setupCollection()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.getAllMovies()
    }
    
    @objc private func refresh() {
        presenter.refresh()
    }
    
    private func endRefresh() {
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
    }
    
    private func setupCollection() {
        
        collection?.refreshControl = refreshControl
        
        collection?.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0)
        
        collection?.register(UINib(nibName: "MovieLibraryCell", bundle: nil),
                             forCellWithReuseIdentifier: MovieLibraryCell.reuseIdentifier)
        
        collection?.delegate = self
        collection?.dataSource = self
    }
}

extension MoviesLibraryViewController: MoviesLibraryPresentable {
    
    func showLoading() {
        
        guard !showingLoading else {
            return
        }
        
        showingLoading = true
        
        present(loading, animated: true, completion: nil)
    }
    
    func hideLoading() {
        
        guard showingLoading else {
            return
        }
        
        loading.dismiss(animated: true) { [weak self] in
            self?.showingLoading = false
        }
    }
    
    func showError(_ error: Error) {
        
        endRefresh()
        
        let alert = UIAlertController(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Aceptar", style: .cancel) { (action) in
            
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showMovies(_ movies: [Movie]) {
        
        endRefresh()
        
        self.movies = movies
        collection?.reloadData()
    }
}

extension MoviesLibraryViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieLibraryCell.reuseIdentifier,
                                                      for: indexPath)
        
        if let movieCell = cell as? MovieLibraryCell {
            
            let movie = movies[indexPath.item]
            
            movieCell.setup(movie: movie)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
     
        let movie = movies[indexPath.item]

        presenter.pushMovieDetail(movie: movie, from: self)
    }
}

extension MoviesLibraryViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemWidth = (collectionView.frame.width - interItemSpacing) / 2.0
        
        let itemHeight = itemWidth / itemAspect
        
        return CGSize(width: itemWidth,
                      height: itemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
     
        return interItemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return lineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return .zero
    }
}
