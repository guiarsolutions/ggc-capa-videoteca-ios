
import UIKit

class MoviesLibraryRouter {
    
    static func start() -> UIViewController {
        
        let presenter = MoviesLibraryPresenter(router: MoviesLibraryRouter())
        
        let controller = MoviesLibraryViewController(presenter: presenter)
        
        return controller
    }
    
    func pushMovieDetail(movie: Movie,
                         from view: UIViewController) {
        
        guard let nav = view.navigationController else {
            return
        }
        
        let movieDetail = MovieDetailRouter.start(movie: movie)
        
        nav.pushViewController(movieDetail, animated: true)
    }
}
