
import UIKit

protocol MoviesLibraryPresentable: UIViewController {
    
    func showLoading()
    func hideLoading()
    func showError(_ error: Error)
    func showMovies(_ movies: [Movie])
}

class MoviesLibraryPresenter {
    
    private let interactor = MoviesLibraryInteractor()
    
    private let router: MoviesLibraryRouter
    
    weak var view: MoviesLibraryPresentable?
    
    private var cache: [Movie]?
    
    init(router: MoviesLibraryRouter) {
        self.router = router
        interactor.delegate = self
    }
    
    func getAllMovies() {
        
        if let movies = cache {
            view?.showMovies(movies)
            return
        }
        
        view?.showLoading()
        
        interactor.getAll()
    }
    
    func pushMovieDetail(movie: Movie,
                         from view: UIViewController) {
        
        router.pushMovieDetail(movie: movie, from: view)
    }
    
    func refresh() {
        
        interactor.getAll()
    }
}

extension MoviesLibraryPresenter: MoviesLibraryInteractorDelegate {
    
    func didFail(error: Error) {
        
        view?.hideLoading()
        
        view?.showError(error)
    }
    
    func didResponse(movies: [Movie]) {
        
        let sorted = movies.sorted(by: {
            if $0.likes == $1.likes {
                return $0.ranking > $1.ranking
            }
            return $0.likes > $1.likes
        })
        
        self.cache = sorted
        
        view?.hideLoading()
        
        view?.showMovies(sorted)
    }
}
