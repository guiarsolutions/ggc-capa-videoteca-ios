
import UIKit

class LoginViewController: UIViewController {

    @IBOutlet var username: UITextField?
    @IBOutlet var dni: UITextField?
    
    @IBOutlet var loginButton: UIButton?
    
    @IBOutlet var loginSuccessLabel: UILabel?
    
    @IBOutlet var loading: UIActivityIndicatorView?
    
    private let presenter: LoginPresenter
    
    init(presenter: LoginPresenter) {
        self.presenter = presenter
        super.init(nibName: "LoginViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        style()
        setup()
    }
    
    private func setup() {
        presenter.view = self
        setupInputs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showWaitForUser()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        tryAutoLogin()
    }
    
    private func style() {
        
        loginButton?.layer.cornerRadius = 5
        loginButton?.layer.masksToBounds = true
    }
    
    private func clearInputs() {
        
        username?.text = ""
        dni?.text = ""
    }
    
    private func setupInputs() {
        
        username?.placeholder = "username"
        
        dni?.placeholder = "dni"
        dni?.keyboardType = .numberPad
        dni?.isSecureTextEntry = true
        
        loginSuccessLabel?.text = "Ingreso exitoso!"
        
        loginButton?.addTarget(self,
                               action: #selector(login),
                               for: .touchUpInside)
    }
    
    private func showWaitForUser() {
        clearInputs()
        enableInputs()
        loginSuccessLabel?.isHidden = true
    }
    
    private func enableInputs() {
        username?.isEnabled = true
        dni?.isEnabled = true
        loginButton?.isHidden = false
    }
    
    private func disableInputs() {
        username?.isEnabled = false
        dni?.isEnabled = false
        loginButton?.isHidden = true
    }
    
    private func tryAutoLogin() {
        
        guard let credentials = UserCredentials.load() else {
            return
        }
        
        username?.text = credentials.username
        dni?.text = credentials.dni
        login()
    }
    
    @objc private func login() {
        
        disableInputs()
        
        let credentials = UserCredentials(username: username?.text ?? "",
                                          dni: dni?.text ?? "")
        
        presenter.login(credentials: credentials)
    }
}

extension LoginViewController : LoginPresentable {
    
    func showLoading() {
    
        loading?.isHidden = false
        loading?.startAnimating()
    }
    
    func hideLoading() {
        
        loading?.stopAnimating()
        loading?.isHidden = true
    }
    
    func showError(_ error: Error) {
                
        let alert = UIAlertController(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Aceptar", style: .cancel) { (action) in
            
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
        
        enableInputs()
    }
    
    func loginSuccessfull(user: User) {
        
        loginSuccessLabel?.isHidden = false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            
            self.presenter.startSession(user: user, from: self)
        }
    }
}
