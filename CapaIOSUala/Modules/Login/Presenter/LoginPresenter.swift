
import UIKit

protocol LoginPresentable: UIViewController {
    
    func showLoading()
    func hideLoading()
    func showError(_ error: Error)
    func loginSuccessfull(user: User)
}

class LoginPresenter {
    
    private let interactor = LoginInteractor()
    
    private let router: LoginRouter
    
    weak var view: LoginPresentable?
    
    init(router: LoginRouter) {
        self.router = router
        interactor.delegate = self
    }
    
    func login(credentials: UserCredentials) {
        
        view?.showLoading()
        
        interactor.execute(credentials: credentials)
    }
    
    func startSession(user: User, from view: UIViewController) {
        
        router.startSession(user: user, from: view)
    }
}

extension LoginPresenter: LoginInteractorDelegate {
    
    func didFail(error: Error) {
        
        view?.hideLoading()
        
        view?.showError(error)
    }
    
    func didLogin(user: User) {
        
        view?.hideLoading()
        
        view?.loginSuccessfull(user: user)
    }
}
