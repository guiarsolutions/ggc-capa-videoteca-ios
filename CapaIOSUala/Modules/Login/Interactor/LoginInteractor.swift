
import RxSwift

protocol LoginInteractorDelegate : class {
    
    func didFail(error: Error)
    func didLogin(user: User)
}

class LoginInteractor {
    
    weak var delegate: LoginInteractorDelegate?
    
    private var disposable: Disposable?
    
    private let repository = UserRepository()
    
    func execute(credentials: UserCredentials) {
        
        disposable?.dispose()
        
        let username = credentials.username
        let dni = credentials.dni
        
        disposable = repository.login(username: username, dni: dni).subscribe(onNext: { [weak self] (user) in
            
            UserCredentials.save(credentials)
            
            self?.delegate?.didLogin(user: user)
            
            }, onError: { [weak self] (error) in
                
                self?.delegate?.didFail(error: error)
                
            }, onCompleted: nil, onDisposed: nil)
    }
}
