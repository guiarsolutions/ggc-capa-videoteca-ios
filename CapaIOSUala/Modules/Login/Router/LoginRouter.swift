
import UIKit

class LoginRouter {
    
    static func start() -> UIViewController {
        
        let presenter = LoginPresenter(router: LoginRouter())
        
        let controller = LoginViewController(presenter: presenter)
        
        return controller
    }
    
    func startSession(user: User, from view: UIViewController) {
 
        Session.start(user: user, from: view)
    }
}
