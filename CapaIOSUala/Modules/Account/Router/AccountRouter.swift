
import UIKit

class AccountRouter {
    
    static func start() -> UIViewController {
        
        let presenter = AccountPresenter(router: AccountRouter())
        
        let controller = AccountViewController(presenter: presenter)
        
        return controller
    }
    
    func pushMovieDetail(movie: Movie,
                         from view: UIViewController) {
        
        guard let nav = view.navigationController else {
            return
        }
        
        let movieDetail = MovieDetailRouter.start(movie: movie)

        nav.pushViewController(movieDetail, animated: true)
    }
    
    func pushEditUser(from view: UIViewController) {
        
        guard let nav = view.navigationController else {
            return
        }
        
        let editUser = EditUserRouter.start()

        nav.pushViewController(editUser, animated: true)
    }
}
