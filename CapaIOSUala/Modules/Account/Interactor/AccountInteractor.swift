
import RxSwift

protocol AccountInteractorDelegate : class {
    
    func didFail(error: Error)
    func didResponse(movies: [Movie])
}

class AccountInteractor {
    
    weak var delegate: AccountInteractorDelegate?
    
    private var disposable: Disposable?
    
    private let repository = MoviesRepository()
    
    func getMoviesLikerForUser(userId: Int) {
        
        disposable?.dispose()
        
        disposable = repository.getMoviesLikedForUser(userId: userId)
            .subscribe(onNext: { [weak self] (movies) in
                
                self?.delegate?.didResponse(movies: movies)
                
                }, onError: { [weak self] (error) in
                    
                    self?.delegate?.didFail(error: error)
                    
                }, onCompleted: nil, onDisposed: nil)
    }
}
