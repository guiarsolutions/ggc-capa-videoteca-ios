
import UIKit

class AccountViewController: UIViewController {
    
    @IBOutlet var fullName: UILabel?
    @IBOutlet var username: UILabel?
    @IBOutlet var age: UILabel?
    @IBOutlet var email: UILabel?
    
    @IBOutlet var table: UITableView?
    
    private let presenter: AccountPresenter
    
    private var movies = [Movie]()
    private var showingLoading = false
    
    private lazy var loading: UIViewController = {
        let loading = UIAlertController(title: "Descargando..",
                                        message: nil,
                                        preferredStyle: .alert)
        return loading
    }()
    
    init(presenter: AccountPresenter) {
        self.presenter = presenter
        super.init(nibName: "AccountViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUser()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.getMoviesLiked()
    }
    
    private func setupUser() {
        
        guard let user = presenter.getUser() else {
            clear()
            return
        }
        
        fullName?.text = "\(user.firstName) \(user.lastName)"
        username?.text = "@\(user.username)"
        email?.text = user.email ?? "..."
        
        age?.text = "--"
        
        if let birthDate = user.birthDate {
            
            let now = Date()
            let calendar = Calendar.current
            
            let ageComponents = calendar.dateComponents([.year],
                                                        from: birthDate,
                                                        to: now)
            
            if let years = ageComponents.year {
                
                age?.text = "\(years) años."
            }
        }
    }
    
    private func clear() {
        
        fullName?.text = "-"
        username?.text = "-"
        age?.text = "-"
        email?.text = "-"
    }
    
    private func setup() {
        
        title = "Mi Cuenta"
        
        clear()
        
        presenter.view = self
        
        setupTable()
        setupNavigationItems()
    }
    
    private func setupTable() {
        
        table?.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0)
        table?.separatorStyle = .none
        
        table?.register(UINib(nibName: "AccountMoviesCell", bundle: nil),
                        forCellReuseIdentifier: AccountMoviesCell.reuseIdentifier)
        table?.delegate = self
        table?.dataSource = self
    }
    
    private func setupNavigationItems() {
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cerrar sesión",
                                                           style: .done,
                                                           target: self,
                                                           action: #selector(logout))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Editar",
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(edit))
    }
    
    @objc private func logout() {
        
        let alert = UIAlertController(title: "Cerrar sesión",
                                      message: "¿Esta seguro?",
                                      preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Aceptar",
                               style: .destructive) { (_) in
                                
                                alert.dismiss(animated: true, completion: {
                                    Session.logout()
                                })
                                
        }
        
        let cancel = UIAlertAction(title: "Cancelar",
                                   style: .default) { (_) in
                                    
                                    alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    @objc private func edit() {
     
        presenter.pushEditUser(from: self)
    }
}

extension AccountViewController : AccountPresentable {
    
    func showLoading() {
        
        guard !showingLoading else {
            return
        }
        
        showingLoading = true
        
        present(loading, animated: true, completion: nil)
    }
    
    func hideLoading() {
        
        guard showingLoading else {
            return
        }
        
        loading.dismiss(animated: true) { [weak self] in
            self?.showingLoading = false
        }
    }
    
    func showError(_ error: Error) {
        
        let alert = UIAlertController(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Aceptar", style: .cancel) { (action) in
            
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showMovies(_ movies: [Movie]) {
        
        self.movies = movies
        table?.reloadData()
    }
}

extension AccountViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat {
        
        return movies.count == 0 ? 60 : 0
    }
    
    func tableView(_ tableView: UITableView,
                   viewForHeaderInSection section: Int) -> UIView? {
        
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 21)
        label.textAlignment = .center
        label.text = "Aun no hay peliculas para mostrar.."
        
        return label
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        
        return movies.count
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: AccountMoviesCell.reuseIdentifier,
                                                 for: indexPath)
        
        if let movieCell = cell as? AccountMoviesCell {
            
            let movie = movies[indexPath.row]
            
            movieCell.setup(movie: movie)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        presenter.pushMovieDetail(movie: movies[indexPath.row], from: self)
    }
}
