
import UIKit

class AccountMoviesCell: UITableViewCell {

    @IBOutlet var cover: UIImageView?
    
    @IBOutlet var title: UILabel?
    @IBOutlet var year: UILabel?
    @IBOutlet var duration: UILabel?
    @IBOutlet var rank: UILabel?
    
    static let reuseIdentifier = "AccountMoviesCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }
    
    func setup(movie: Movie) {
        
        if let coverImageUrl = movie.coverImageUrl {
            cover?.sd_setImage(with: URL(string: coverImageUrl), completed: nil)
        }
        title?.text = movie.title
        year?.text = "\(movie.releaseYear)"
        rank?.text = "\(movie.ranking)"
        
        let h = movie.duration / 60
        let m = movie.duration % 60
        duration?.text = "\(h)h \(m)min"
    }
    
    private func setup() {
        
        prepareForReuse()
        
        cover?.layer.cornerRadius = 3
        cover?.layer.borderWidth = 1
        cover?.layer.borderColor = UIColor.black.cgColor
        cover?.layer.masksToBounds = true
    }
    
    private func clear() {
        
        cover?.image = nil
        title?.text = "-"
        year?.text = "-"
        duration?.text = "-"
        rank?.text = "-"
    }
}
