
import UIKit

protocol AccountPresentable: UIViewController {
    
    func showLoading()
    func hideLoading()
    func showError(_ error: Error)
    func showMovies(_ movies: [Movie])
}

class AccountPresenter {
    
    private let interactor = AccountInteractor()
    
    private let router: AccountRouter
    
    weak var view: AccountPresentable?
        
    init(router: AccountRouter) {
        self.router = router
        interactor.delegate = self
    }
    
    func getUser() -> User? {
        return Session.getUser()
    }
    
    func getMoviesLiked() {
        
        guard let user = getUser() else {
            return
        }
        
        view?.showLoading()
        
        interactor.getMoviesLikerForUser(userId: user.id)
    }
    
    func pushMovieDetail(movie: Movie,
                         from view: UIViewController) {
        
        router.pushMovieDetail(movie: movie, from: view)
    }
    
    func pushEditUser(from view: UIViewController) {
        
        router.pushEditUser(from: view)
    }
}

extension AccountPresenter: AccountInteractorDelegate {
    
    func didFail(error: Error) {
        
        view?.hideLoading()
        
        view?.showError(error)
    }
    
    func didResponse(movies: [Movie]) {
        
        let sorted = movies.sorted(by: {
            if $0.likes == $1.likes {
                return $0.ranking > $1.ranking
            }
            return $0.likes > $1.likes
        })
                
        view?.hideLoading()
        
        view?.showMovies(sorted)
    }
}
