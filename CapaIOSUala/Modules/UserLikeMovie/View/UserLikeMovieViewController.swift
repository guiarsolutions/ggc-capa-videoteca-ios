
import UIKit
import Cosmos

protocol UserLikeMovieChangesDelegate : class {
    
    func didChangeMovieDetail(_ movieDetail: MovieDetail)
}

class UserLikeMovieViewController: UIViewController {
    
    @IBOutlet var movie: UILabel?
    
    @IBOutlet var voteContainer: UIView?
    
    @IBOutlet var sendButton: UIButton?
    
    @IBOutlet var cancelButton: UIButton?
    
    @IBOutlet var rankingView: CosmosView?
    
    @IBOutlet var loading: UIActivityIndicatorView?
    
    private let presenter: UserLikeMoviePresenter
    
    private weak var delegate: UserLikeMovieChangesDelegate?
    
    init(presenter: UserLikeMoviePresenter,
         delegate: UserLikeMovieChangesDelegate) {
        self.delegate = delegate
        self.presenter = presenter
        super.init(nibName: "UserLikeMovieViewController", bundle: nil)
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        
        hideLoading()
        
        presenter.view = self
        
        voteContainer?.layer.cornerRadius = 5
        voteContainer?.layer.shadowRadius = 5
        voteContainer?.layer.shadowOffset = CGSize(width: 4, height: 4)
        voteContainer?.layer.shadowColor = UIColor.black.cgColor
        voteContainer?.layer.shadowOpacity = 0.7
        
        sendButton?.layer.cornerRadius = 5
        sendButton?.layer.masksToBounds = true
        
        cancelButton?.addTarget(self,
                                action: #selector(close),
                                for: .touchUpInside)
        
        sendButton?.addTarget(self,
                              action: #selector(sendVote),
                              for: .touchUpInside)
    }
    
    @objc private func close() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func sendVote() {
        
        guard let value = rankingView?.rating else {
            return
        }
        
        presenter.voteMovie(value: value)
    }
}

extension UserLikeMovieViewController : UserLikeMoviePresentable {
    
    func showLoading() {
        
        sendButton?.isHidden = true
        loading?.isHidden = false
        loading?.startAnimating()
    }
    
    func hideLoading() {
        
        loading?.stopAnimating()
        loading?.isHidden = true
        sendButton?.isHidden = false
    }
    
    func showError(_ error: Error) {
        
        let alert = UIAlertController(title: "Error",
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Aceptar", style: .cancel) { (action) in
            
            alert.dismiss(animated: true, completion: {
            
                self.close()
            })
        }
        
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    func didVoteMovieSuccessfull(movieDetail: MovieDetail?) {
        
        guard let detail = movieDetail else {
            return
        }
        
        delegate?.didChangeMovieDetail(detail)
        
        close()
    }
    
    func setup(movieTitle: String,
               userRanking: Double) {
        
        movie?.text = movieTitle
        rankingView?.rating = userRanking
    }
}
