
import UIKit

class UserLikeMovieRouter {
    
    static func start(movie: Movie,
                      userMovie: UserMovie,
                      delegate: UserLikeMovieChangesDelegate) -> UIViewController {
        
        let presenter = UserLikeMoviePresenter(movie: movie,
                                               userMovie: userMovie,
                                               router: UserLikeMovieRouter())
        
        let controller = UserLikeMovieViewController(presenter: presenter,
                                                     delegate: delegate)
        
        return controller
    }
}
