
import UIKit

protocol UserLikeMoviePresentable : UIViewController {
    
    func showLoading()
    func hideLoading()
    func showError(_ error: Error)
    func setup(movieTitle: String, userRanking: Double)
    func didVoteMovieSuccessfull(movieDetail: MovieDetail?)
}

class UserLikeMoviePresenter {
    
    private let movie: Movie
    private let userMovie: UserMovie
    private let router: UserLikeMovieRouter
    
    weak var view: UserLikeMoviePresentable? {
        didSet {
            view?.setup(movieTitle: movie.title, userRanking: userMovie.vote)
        }
    }
    
    private let interactor = UserLikeMovieInteractor()
    
    init(movie: Movie,
         userMovie: UserMovie,
         router: UserLikeMovieRouter) {
     
        self.movie = movie
        self.userMovie = userMovie
        self.router = router
        interactor.delegate = self
    }
    
    func voteMovie(value: Double) {
        
        guard let user = Session.getUser() else {
            return
        }
        
        view?.showLoading()
        
        interactor.vote(movieId: movie.id, userId: user.id, vote: value)
    }
}

extension UserLikeMoviePresenter : UserLikeMovieInteractorDelegate {
    
    func didFail(error: Error) {

        view?.showError(error)
    }
    
    func didResponse(detail: MovieDetail?) {
        
        view?.didVoteMovieSuccessfull(movieDetail: detail)
    }
}
