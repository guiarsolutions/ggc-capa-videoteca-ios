
import RxSwift

protocol UserLikeMovieInteractorDelegate : class {
    
    func didFail(error: Error)
    func didResponse(detail: MovieDetail?)
}

class UserLikeMovieInteractor {
    
    weak var delegate: UserLikeMovieInteractorDelegate?
    
    private var disposable: Disposable?
    
    private let repository = MoviesRepository()
    
    func vote(movieId: Int,
              userId: Int,
              vote: Double) {
        
        disposable?.dispose()
        
        disposable = repository.voteMovie(movieId: movieId,
                                          userId: userId,
                                          vote: vote)
            .subscribe(onNext: { [weak self] (detail) in
                
                self?.delegate?.didResponse(detail: detail)
                
                }, onError: { [weak self] (error) in
                    
                    self?.delegate?.didFail(error: error)
                    
                }, onCompleted: nil, onDisposed: nil)
    }
}
