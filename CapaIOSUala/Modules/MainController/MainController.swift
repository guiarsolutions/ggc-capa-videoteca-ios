
import UIKit

class MainController: UITabBarController {
    
    private lazy var library = createController(MoviesLibraryRouter.start(),
                                                title: "Videoteca",
                                                unselectedImage: UIImage(named: "tab-icon-movies"),
                                                selectedImage: UIImage(named: "tab-icon-movies-filled"))
    
    private lazy var search = createController(UIViewController(),
                                               title: "Buscar",
                                               unselectedImage: UIImage(named: "tab-icon-search"),
                                               selectedImage: UIImage(named: "tab-icon-search-filled"))
    
    private lazy var profile = createController(AccountRouter.start(),
                                                title: "Mi Cuenta",
                                                unselectedImage: UIImage(named: "tab-icon-profile"),
                                                selectedImage: UIImage(named: "tab-icon-profile-filled"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        setupTabBar()
        setupControllers()
    }
    
    private func setupTabBar() {
        tabBar.barStyle = .black
        tabBar.isTranslucent = true
        tabBar.tintColor = .white
        tabBar.barTintColor = .black
    }
    
    private func setupControllers() {
        viewControllers = [library, search, profile]
    }
}

extension MainController {
    
    private func createController(_ controller: UIViewController,
                                  title: String?,
                                  unselectedImage: UIImage?,
                                  selectedImage: UIImage?) -> UIViewController {
        
        let nav = UINavigationController(rootViewController: controller)
        nav.title = title
        nav.tabBarItem.image = unselectedImage
        nav.tabBarItem.selectedImage = selectedImage
        return nav
    }
}
