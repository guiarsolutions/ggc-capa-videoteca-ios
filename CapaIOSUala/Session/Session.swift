
import Foundation
import UIKit

class Session {
    
    private static let shared = Session()
    
    private init() {}
    
    private var user: User?
    
    static func getUser() -> User? {
        return shared.user
    }
    
    static func updateUser(_ user: User) {
        shared.user = user
    }
    
    private var mainController: UIViewController?
    
    static func start(user: User, from view: UIViewController) {
        
        let mainController = MainController()
        mainController.modalPresentationStyle = .fullScreen

        shared.user = user
        
        view.present(mainController, animated: true, completion: {
            shared.mainController = mainController
        })
    }
    
    static func logout() {
        
        UserCredentials.clear()
        
        shared.mainController?.dismiss(animated: true, completion: {
            shared.mainController = nil
        })
    }
}

