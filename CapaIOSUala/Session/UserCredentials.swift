
import Foundation

class UserCredentials {
    
    let username: String
    let dni: String
    
    private static let usernameKey = "UserCredentials-username"
    private static let dniKey = "UserCredentials-dni"
    
    init(username: String,
         dni: String) {
        
        self.username = username
        self.dni = dni
    }
    
    static func load() -> UserCredentials? {
        
        let ud = UserDefaults.standard
        
        guard
            let username = ud.value(forKey: usernameKey) as? String,
            let dni = ud.value(forKey: dniKey) as? String else {
                return nil
        }
        
        return UserCredentials(username: username, dni: dni)
    }
    
    static func save(_ credentials: UserCredentials) {
    
        let ud = UserDefaults.standard
        ud.setValue(credentials.username, forKey: usernameKey)
        ud.setValue(credentials.dni, forKey: dniKey)
        ud.synchronize()
    }
    
    static func clear() {
        
        let ud = UserDefaults.standard
        ud.removeObject(forKey: usernameKey)
        ud.removeObject(forKey: dniKey)
        ud.synchronize()
    }
}
